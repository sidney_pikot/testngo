<?php

namespace App\Http\Controllers\Api;
use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminAuthController extends Controller
{
    //
    public function register(Request $request){
        $validateData = $request->validate([
            'username'=>'required|unique:admins',
            'password'=>'required|confirmed'
        ]);
        $admin = Admin::create($validateData);

        $accessToken = $admin->createToken('authToken')->accessToken;

        return response(['admin'=> $admin, 'access_token'=> $accessToken]);
    }
}
